package com.task.evotor.balakine.evotorTask.service;

import com.task.evotor.balakine.evotorTask.common.util.Request;
import com.task.evotor.balakine.evotorTask.common.util.RequestType;
import com.task.evotor.balakine.evotorTask.common.util.Response;
import com.task.evotor.balakine.evotorTask.common.util.ResponseType;
import com.task.evotor.balakine.evotorTask.service.exception.ParserException;
import com.task.evotor.balakine.evotorTask.service.exception.XmlValidationException;
import com.task.evotor.balakine.evotorTask.service.util.DomParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DomParser.class})
public class DomParserTest {

    @Autowired
    private DomParser parser;

    @Test
    public void unmarshal() throws XmlValidationException {
        String xmlStringToUnmarshal = "<?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                "<request>\n" +
                "<request-type>GET-BALANCE</request-type> \n" +
                "<extra name=\"login\">login</extra> \n" +
                "<extra name=\"password\">password</extra> \n" +
                "</request>";
        Request actual = parser.unmarshal(xmlStringToUnmarshal);

        Request expected = Request.builder()
                .login("login")
                .password("password")
                .type(RequestType.GET_BALANCE)
                .build();
        assertEquals(expected, actual);
    }

    @Test
    public void marshalWithOutBalance() throws ParserException {
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<response>" +
                "<result-code>0</result-code>" +
                "</response>";
        Response response = Response.builder()
                .type(ResponseType.OK)
                .build();
        String actual = parser.marshal(response);
        assertEquals(expected, actual);

    }

    @Test
    public void marshalWithBalance() throws ParserException {
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<response>" +
                "<result-code>0</result-code>" +
                "<extra name=\"balance\">100.00</extra>" +
                "</response>";
        Response response = Response.builder()
                .type(ResponseType.OK)
                .balance(new BigDecimal(100))
                .build();
        String actual = parser.marshal(response);
        assertEquals(expected, actual);
    }

    @Test(expected = XmlValidationException.class)
    public void unmarshalInvalidXml() throws XmlValidationException {
        String invalidXml = " <?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                "<invalidTag></invalidTag>" +
                "<response> \n" +
                "<result-code>0</result-code> \n" +
                "</response>";
        parser.unmarshal(invalidXml);
    }
}
