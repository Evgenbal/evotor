package com.task.evotor.balakine.evotorTask.service;

import com.task.evotor.balakine.evotorTask.common.model.Account;
import com.task.evotor.balakine.evotorTask.common.util.Response;
import com.task.evotor.balakine.evotorTask.common.util.ResponseType;
import com.task.evotor.balakine.evotorTask.dao.AccountDao;
import com.task.evotor.balakine.evotorTask.service.impl.AccountServiceImpl;
import com.task.evotor.balakine.evotorTask.service.util.SHA256;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountServiceImplTest {
    @Mock
    private AccountDao accountDao;

    @InjectMocks
    private AccountServiceImpl accountService;

    private Account firstAccount;


    @Before
    public void init() {


        firstAccount = Account.builder()
                .accountId(1L)
                .login("firstTestLogin")
                .password("1111")
                .balance(1111d)
                .build();
    }

    @Test
    public void foundAccountWithValidLogin() {
        Mockito.when(accountDao.findAccountByLogin(firstAccount.getLogin())).thenReturn(firstAccount);
        Account accountFounded = accountService.readByLogin(firstAccount);
        assertEquals(accountFounded, firstAccount);

    }

    @Test
    public void getBalanceWithInvalidLogin() {
        Account invalidAccount = Account.builder()
                .login("invalidLogin")
                .build();

        Mockito.when(accountDao.findAccountByLogin(invalidAccount.getLogin())).thenReturn(null);

        Response expected = Response.builder().type(ResponseType.USER_NOT_EXIST).build();

        Response actual = accountService.getBalance(invalidAccount);

        assertEquals(expected, actual);
    }

    @Test
    public void getBalanceInvalidPassword() {
        Account invalidPassword = Account.builder()
                .login("firstTestLogin")
                .password("invalidPassword")
                .build();
        firstAccount.setPassword(SHA256.hashPw(firstAccount.getPassword()));
        Mockito.when(accountDao.findAccountByLogin(invalidPassword.getLogin())).thenReturn(firstAccount);

        Response expected = Response.builder().type(ResponseType.PASS_INCORRECT).build();
        Response actual = accountService.getBalance(invalidPassword);

        assertEquals(expected, actual);

    }

    @Test
    public void getBalanceOk() {
        Account testAccount = Account.builder()
                .accountId(1L)
                .login("firstTestLogin")
                .password("1111")
                .balance(1111d)
                .build();

        firstAccount.setPassword(SHA256.hashPw(firstAccount.getPassword()));
        Mockito.when(accountDao.findAccountByLogin(firstAccount.getLogin())).thenReturn(firstAccount);

        Response expected = Response.builder().type(ResponseType.OK).balance(new BigDecimal(1111)).build();
        Response actual = accountService.getBalance(testAccount);

        assertEquals(expected, actual);

    }

    @Test
    public void registrationWithDuplicateLogin() {
        Account accountToRegistration = Account.builder()
                .login("firstTestLogin")
                .build();
        Mockito.when(accountDao.findAccountByLogin(firstAccount.getLogin())).thenReturn(firstAccount);

        Response expected = Response.builder().type(ResponseType.DUPLICATE_LOGIN).build();
        Response actual = accountService.registration(accountToRegistration);

        assertEquals(expected, actual);
    }

    @Test
    public void registrationOk() {
        Account accountToRegistration = Account.builder()
                .login("newLogin")
                .password("1234")
                .build();
        Mockito.when(accountDao.findAccountByLogin(firstAccount.getLogin())).thenReturn(firstAccount);

        Response expected = Response.builder().type(ResponseType.OK).build();
        Response actual = accountService.registration(accountToRegistration);

        assertEquals(expected, actual);
    }

}
