package com.task.evotor.balakine.evotorTask.webapp;

import com.task.evotor.balakine.evotorTask.common.util.Request;
import com.task.evotor.balakine.evotorTask.common.util.RequestType;
import com.task.evotor.balakine.evotorTask.common.util.Response;
import com.task.evotor.balakine.evotorTask.common.util.ResponseType;
import com.task.evotor.balakine.evotorTask.controller.MainController;
import com.task.evotor.balakine.evotorTask.service.AccountService;
import com.task.evotor.balakine.evotorTask.service.exception.XmlValidationException;
import com.task.evotor.balakine.evotorTask.service.util.DomParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(MainController.class)
public class MainControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    @MockBean
    private DomParser parser;

    @MockBean
    private HttpServletResponse response;

    @Test
    public void getBalanceOK() throws Exception {

        String requestForBalance = "<?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                "            <request>\n" +
                "            <request-type>GET-BALANCE</request-type> \n" +
                "            <extra name=\"login\">login</extra> \n" +
                "            <extra name=\"password\">password</extra> \n" +
                "            </request>";

        String expectedResponseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<response>" +
                "<result-code>0</result-code>" +
                "<extra name=\"balance\">100.00</extra>" +
                "</response>";

        when(parser.unmarshal(anyString())).thenReturn(
                Request.builder()
                        .login("login")
                        .password("password")
                        .type(RequestType.GET_BALANCE)
                        .build());

        when(accountService.getBalance(any())).thenReturn(
                Response.builder()
                        .type(ResponseType.OK)
                        .balance(new BigDecimal(100))
                        .build());


        PrintWriter writer = new PrintWriter(new StringWriter());

        when(response.getWriter()).thenReturn(writer);

        when(parser.marshal(any())).thenReturn(expectedResponseXml);

        MvcResult mvcResult = mvc.perform(post("/process")
                .contentType("application/xml")
                .content(requestForBalance))
                .andDo(print())
                .andReturn();

        String actualResponseXml = mvcResult.getResponse().getContentAsString();

        assertEquals(expectedResponseXml, actualResponseXml);
    }

    @Test
    public void technicalError() throws Exception {
        String requestTechnicalError = "<?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                "            <request>\n" +
                "<invalidTag></invalidTag>" +
                "            <request-type>GET-BALANCE</request-type> \n" +
                "            <extra name=\"login\">login</extra> \n" +
                "            <extra name=\"password\">password</extra> \n" +
                "            </request>";

        String expectedResponseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<response>" +
                "<result-code>2</result-code>" +
                "</response>";

        when(parser.unmarshal(anyString()))
                .thenThrow(XmlValidationException.class);

        PrintWriter writer = new PrintWriter(new StringWriter());

        when(response.getWriter()).thenReturn(writer);

        when(parser.marshal(any())).thenReturn(expectedResponseXml);

        MvcResult mvcResult = mvc.perform(post("/process")
                .contentType("application/xml")
                .content(requestTechnicalError))
                .andDo(print())
                .andReturn();

        String actualResponseXml = mvcResult.getResponse().getContentAsString();

        assertEquals(expectedResponseXml, actualResponseXml);
    }

    @Test
    public void registrationOK() throws Exception {

        String requestForBalance = "<?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                "            <request>\n" +
                "            <request-type>CREATE-AGT</request-type> \n" +
                "            <extra name=\"login\">login</extra> \n" +
                "            <extra name=\"password\">password</extra> \n" +
                "            </request>";

        String expectedResponseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<response>" +
                "<result-code>0</result-code>" +
                "</response>";

        when(parser.unmarshal(anyString())).thenReturn(
                Request.builder()
                        .login("login")
                        .password("password")
                        .type(RequestType.CREATE_AGT)
                        .build());

        when(accountService.registration(any())).thenReturn(
                Response.builder()
                        .type(ResponseType.OK)
                        .build());


        PrintWriter writer = new PrintWriter(new StringWriter());
        when(response.getWriter()).thenReturn(writer);

        when(parser.marshal(any())).thenReturn(expectedResponseXml);

        MvcResult mvcResult = mvc.perform(post("/process")
                .contentType("application/xml")
                .content(requestForBalance))
                .andDo(print())
                .andReturn();

        String actualResponseXml = mvcResult.getResponse().getContentAsString();

        assertEquals(expectedResponseXml, actualResponseXml);
    }
}
