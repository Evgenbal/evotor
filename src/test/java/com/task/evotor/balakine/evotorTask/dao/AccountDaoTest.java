package com.task.evotor.balakine.evotorTask.dao;

import com.task.evotor.balakine.evotorTask.common.model.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class AccountDaoTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountDao accountDao;

    private Account firstAccount;
    private Account secondAccount;
    private Account thirdAccount;


    @Before
    public void init() {


        firstAccount = Account.builder()
                .login("firstTestLogin")
                .password("1111")
                .balance(1111d)
                .build();

        secondAccount = Account.builder()
                .login("secondTestLogin")
                .password("2222")
                .balance(2222d)
                .build();


        thirdAccount = Account.builder()
                .login("thirdTestLogin")
                .password("1111")
                .balance(3333d)
                .build();



    }


    @Test
    public void createOk() {
        Account accountSaved = accountDao.save(firstAccount);
        entityManager.flush();

        Account account = entityManager.find(Account.class, accountSaved.getId());

        assertEquals(firstAccount, account);
    }

    @Test
    public void readById() {
        long id = entityManager.persistAndGetId(firstAccount, Long.class);
        entityManager.flush();

        Account account = accountDao.findOne(id);

        assertEquals(firstAccount, account);

    }


    @Test
    public void readByLogin() {
        entityManager.persist(firstAccount);
        entityManager.persist(secondAccount);
        entityManager.persist(thirdAccount);
        entityManager.flush();

        Account found = accountDao.findAccountByLogin(secondAccount.getLogin());

        assertEquals(secondAccount, found);

    }


    @Test
    public void readByLoginNull() {
        entityManager.persistAndFlush(firstAccount);

        Account found = accountDao.findAccountByLogin("randomMail");

        assertNull(found);
    }

    @Test
    public void update() {
        entityManager.persistAndFlush(firstAccount);
        firstAccount.setLogin("NewName");
        accountDao.save(firstAccount);
        Account account = entityManager.find(Account.class, firstAccount.getId());

        assertEquals(firstAccount.getLogin(), account.getLogin());
    }


    @Test
    public void deleteById() {
        long id = entityManager.persistAndGetId(firstAccount, Long.class);
        entityManager.flush();

        accountDao.delete(id);
        Account actual = accountDao.findOne(id);
        assertNull(actual);
    }

    @Test
    public void deleteByObject() {
        entityManager.persistAndFlush(firstAccount);

        accountDao.delete(firstAccount);
        Account actual = accountDao.findAccountByLogin(firstAccount.getLogin());
        assertNull(actual);
    }
}
