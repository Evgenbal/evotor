CREATE TABLE ACCOUNT_TBL
(
  account_id BIGINT        NOT NULL AUTO_INCREMENT,
  login      VARCHAR(100),
  password   VARCHAR(200)  NOT NULL,
  balance    DOUBLE(20, 2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (account_id),
  UNIQUE (login)
);