package com.task.evotor.balakine.evotorTask.common.util;

import lombok.*;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class Response {

    private ResponseType type;
    private BigDecimal balance;
}
