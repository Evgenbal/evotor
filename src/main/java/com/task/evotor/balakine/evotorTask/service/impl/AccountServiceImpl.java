package com.task.evotor.balakine.evotorTask.service.impl;

import com.task.evotor.balakine.evotorTask.common.util.Response;
import com.task.evotor.balakine.evotorTask.common.util.ResponseType;
import com.task.evotor.balakine.evotorTask.common.model.Account;
import com.task.evotor.balakine.evotorTask.dao.AccountDao;
import com.task.evotor.balakine.evotorTask.service.AccountService;
import com.task.evotor.balakine.evotorTask.service.util.SHA256;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class AccountServiceImpl extends AbstractService<Account> implements AccountService{
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    private final AccountDao accountDao;

    @Autowired
    public AccountServiceImpl(AccountDao accountDao) {
        super(accountDao);
        this.accountDao = accountDao;
    }
    @Override
    public Account readByLogin(Account accountToFind) {
        logger.debug("start find entity by login with login: " + accountToFind.getLogin());
        Account foundedAccount = accountDao.findAccountByLogin(accountToFind.getLogin());
        logger.debug("account with login " + accountToFind.getLogin() + " found. Id: " + foundedAccount.getId());
        return foundedAccount;
    }

    @Override
    public Response getBalance(Account account) {
        logger.debug("Start retrieve balance for login: " + account.getLogin());
        Response response = new Response();
        Account accountFromDataBase = accountDao.findAccountByLogin(account.getLogin());
        if (accountFromDataBase == null) {
            response.setType(ResponseType.USER_NOT_EXIST);
            logger.debug("Account with login: " + account.getLogin() + " doesn't exist");
            return response;
        }
        if (!accountFromDataBase.getPassword().equals(SHA256.hashPw(account.getPassword()))) {
            response.setType(ResponseType.PASS_INCORRECT);
            logger.debug("Incorrect password for: " + account);
            return response;
        }
        response.setBalance(new BigDecimal(accountFromDataBase.getBalance()));
        response.setType(ResponseType.OK);
        logger.debug("Response with balance created " + response);
        return response;
    }

    @Override
    @Transactional
    public Response registration(Account account) {
        logger.debug("Start registering " + account.getLogin());
        Response response = new Response();
        Account accountFromDataBase = accountDao.findAccountByLogin(account.getLogin());
        if (accountFromDataBase != null) {
            response.setType(ResponseType.DUPLICATE_LOGIN);
            logger.debug("Account with login " + account.getLogin() + " already exist");
            return response;
        }
        account.setPassword(SHA256.hashPw(account.getPassword()));
        accountDao.save(account);
        response.setType(ResponseType.OK);
        logger.debug("Registration response created " + response);
        return response;

    }
}
