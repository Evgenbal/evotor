package com.task.evotor.balakine.evotorTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvotorTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvotorTaskApplication.class, args);
	}
}
