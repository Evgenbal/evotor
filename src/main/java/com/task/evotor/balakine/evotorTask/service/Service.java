package com.task.evotor.balakine.evotorTask.service;

import com.task.evotor.balakine.evotorTask.common.AbstractModel;

public interface Service<T extends AbstractModel> {
    /**
     * Make an instance managed and persistent.
     *
     * @param obj - entity instance
     * @return  created object
     * @see javax.persistence.EntityManager
     */
    T create(T obj);

    /**
     * Getting entity from database by id
     *
     * @param id - id in database
     * @return object with id
     */
    T read(Long id);

    /**
     * Merge the state of the given entity into the
     * current persistence context.
     *
     * @param obj entity instance
     * @return the managed instance that the state was merged to
     * @see javax.persistence.EntityManager
     */
    void update(T obj);

    /**
     * Remove the entity instance.
     *
     * @param id -  id of entity instance
     * @see javax.persistence.EntityManager
     */
    void delete(Long id);

}
