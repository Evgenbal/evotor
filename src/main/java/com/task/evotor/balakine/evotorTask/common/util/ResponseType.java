package com.task.evotor.balakine.evotorTask.common.util;

import java.util.Arrays;

public enum ResponseType {
    OK(0),
    DUPLICATE_LOGIN(1),
    TECHNICAL_ERROR(2),
    USER_NOT_EXIST(3),
    PASS_INCORRECT(4);

    private final int type;

    ResponseType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
