package com.task.evotor.balakine.evotorTask.common.util;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class Request {
    private RequestType type;
    private String login;
    private String password;
}
