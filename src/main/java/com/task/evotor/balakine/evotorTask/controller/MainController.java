package com.task.evotor.balakine.evotorTask.controller;

import com.task.evotor.balakine.evotorTask.common.util.Request;
import com.task.evotor.balakine.evotorTask.common.util.Response;
import com.task.evotor.balakine.evotorTask.common.util.ResponseType;
import com.task.evotor.balakine.evotorTask.common.model.Account;
import com.task.evotor.balakine.evotorTask.service.AccountService;
import com.task.evotor.balakine.evotorTask.service.exception.ParserException;
import com.task.evotor.balakine.evotorTask.service.exception.XmlValidationException;
import com.task.evotor.balakine.evotorTask.service.util.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Optional;

@Controller
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    private final AccountService accountService;

    private final Parser parser;

    @Autowired
    public MainController(AccountService accountService, Parser parser) {
        this.accountService = accountService;
        this.parser = parser;
    }


    @GetMapping(value = "/")
    public ModelAndView mainPage() {
        logger.debug("Start load main page");
        return new ModelAndView("main");
    }

    @PostMapping(value = "/process", produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public void processRequest(@RequestBody String xmlRequest, HttpServletResponse response) {
        logger.debug("Start processing request: " + xmlRequest);
        Request request;
        Response responseToSerialize = null;

        try {
            request = parser.unmarshal(xmlRequest);
        } catch (XmlValidationException e) {
            e.printStackTrace();
            responseToSerialize = Response.builder().type(ResponseType.TECHNICAL_ERROR).build();
            writeResponse(response, responseToSerialize);
            return;
        }

        Account account = Account.builder()
                .login(request.getLogin())
                .password(request.getPassword())
                .build();
        switch (request.getType()) {
            case CREATE_AGT:
                logger.debug("CREATE-AGT request type");
                responseToSerialize = accountService.registration(account);
                break;
            case GET_BALANCE:
                logger.debug("GET-BALANCE request type");
                responseToSerialize = accountService.getBalance(account);
                break;

        }
        Response responseOptional = Optional.of(responseToSerialize)
                .orElse(Response.builder().type(ResponseType.TECHNICAL_ERROR).build());

        writeResponse(response, responseOptional);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public void handleException(Writer writer) throws IOException {
        logger.debug("Handle exception " + DataIntegrityViolationException.class);
        Response response = Response.builder().type(ResponseType.DUPLICATE_LOGIN).build();
        writer.write(parseResponse(response));
        logger.debug("Response marshaled" + response);
    }

    private void writeResponse(HttpServletResponse response, Response responseToSerialize) {
        try (PrintWriter out = response.getWriter()) {
            logger.debug(responseToSerialize + "ready for parsing");
            response.setContentType("application/xml;charset=UTF-8");
            String result = parseResponse(responseToSerialize);
            out.write(result);
            logger.debug(responseToSerialize + " marshaled; " + result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String parseResponse(Response responseToSerialize) {
        String result = "";
        try {
            result = parser.marshal(responseToSerialize);
        } catch (ParserException e) {
            e.printStackTrace();
        }
        return result;
    }
}
