package com.task.evotor.balakine.evotorTask.service.exception;

public class XmlValidationException extends Exception {
    public XmlValidationException(Throwable cause) {
        super(cause);
    }
}
