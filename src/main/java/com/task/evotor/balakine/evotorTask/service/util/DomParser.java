package com.task.evotor.balakine.evotorTask.service.util;

import com.task.evotor.balakine.evotorTask.common.util.Request;
import com.task.evotor.balakine.evotorTask.common.util.RequestType;
import com.task.evotor.balakine.evotorTask.common.util.Response;
import com.task.evotor.balakine.evotorTask.service.exception.ParserException;
import com.task.evotor.balakine.evotorTask.service.exception.XmlValidationException;
import com.task.evotor.balakine.evotorTask.service.impl.AbstractService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.w3c.dom.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class DomParser implements Parser {
    private static final Logger logger = LoggerFactory.getLogger(DomParser.class);

    private static final String XSD_FILE_NAME = "XSDSchema.xsd";

    @Override
    public String marshal(Response response) throws ParserException {
        try (StringWriter stringWriter = new StringWriter()) {
            logger.debug("Start marshal: " + response);
            //create Builder
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();

            doc.setXmlStandalone(true);

            // root element
            Element responseElement = doc.createElement("response");
            doc.appendChild(responseElement);
            logger.debug("root tag created " + responseElement.getNodeName());

            // result-code element
            Element resultCode = doc.createElement("result-code");
            resultCode.appendChild(doc.createTextNode(String.valueOf(response.getType().getType())));
            responseElement.appendChild(resultCode);
            logger.debug("result-code tag created " + resultCode.getNodeName());

            // extra element
            if (response.getBalance() != null) {
                logger.debug("extra balance is present. Balance: " + response.getBalance());
                Element extra = doc.createElement("extra");
                BigDecimal balanceWithTwoDecimal = response.getBalance().setScale(2, RoundingMode.HALF_UP);
                extra.appendChild(doc.createTextNode(String.valueOf(balanceWithTwoDecimal)));
                responseElement.appendChild(extra);

                Attr attr = doc.createAttribute("name");
                attr.setValue("balance");
                extra.setAttributeNode(attr);
                logger.debug("balance created");
            }


            // write the content
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(stringWriter);

            transformer.transform(source, result);


            String resultXml = stringWriter.toString();
            logger.debug("Xml String created " + resultXml);
            return resultXml;

        } catch (ParserConfigurationException | TransformerException pce) {
            throw new ParserException(pce);
        } catch (IOException e) {
            throw new ParserException(e);
        }

    }

    @Override
    public Request unmarshal(String inputXmlString) throws XmlValidationException {
        validate(inputXmlString);
        try (InputStream inputStream = IOUtils.toInputStream(inputXmlString)) {
            //create dom factory
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbFactory.newDocumentBuilder();
            logger.debug("Dom builder initialized");
            Document document = builder.parse(inputStream);

            document.getDocumentElement().normalize();

            return parsing(document);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Request parsing(Document document) {
        Request request = null;
        Node mainNode = document.getDocumentElement();
        if (mainNode != null && mainNode.getNodeName().equals("request")) {
            logger.debug("Root node found: " + mainNode.getNodeName());
            request = new Request();
            logger.debug("Request initialized");
            NodeList cNodes = mainNode.getChildNodes();
            logger.debug("Size of root child nodes: " + cNodes.getLength());
            for (int j = 0; j < cNodes.getLength(); j++) {
                Node cNode = cNodes.item(j);
                if (cNode != null && cNode instanceof Element) {
                    logger.debug("start handle " + ((Element) cNode).getTagName());
                    String content = cNode.getTextContent().trim();
                    switch (cNode.getNodeName()) {
                        case ("request-type"):
                            logger.debug("Request-type node found");
                            request.setType(RequestType.getRequestType(content));
                            break;
                        case ("extra"):
                            logger.debug("extra node found");
                            String attributeValue = ((Element) cNode).getAttribute("name");
                            logger.debug("Attribute of extra: " + attributeValue);
                            switch (attributeValue) {
                                case ("login"):
                                    request.setLogin(content);
                                    break;
                                case ("password"):
                                    request.setPassword(content);
                                    break;
                            }
                            break;
                    }
                }

            }
        }
        logger.debug("Finished parsing request " + request);
        return request;
    }

    private void validate(String xmlString) throws XmlValidationException {
        ClassPathResource resource = new ClassPathResource(XSD_FILE_NAME);
        try (Reader fileReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
             Reader stringReader = new BufferedReader(new StringReader(xmlString))) {

            logger.debug("Validate xml string " + xmlString);
            Source xsdFile = new StreamSource(fileReader);
            Source xmlFile = new StreamSource(stringReader);

            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(xsdFile);
            Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            logger.debug("Xml string " + xmlString + " valid");
        } catch (Exception e) {
            logger.debug("Xml string " + xmlString + " invalid");
            throw new XmlValidationException(e);
        }

    }


}


