package com.task.evotor.balakine.evotorTask.common.util;

import java.util.Arrays;

public enum RequestType {
    CREATE_AGT("CREATE-AGT"),
    GET_BALANCE("GET-BALANCE");

    private final String type;

    RequestType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    private static RequestType[] requestTypesValues = RequestType.values();

    public static RequestType getRequestType(String type) {
        return Arrays.stream(requestTypesValues).filter(t -> t.type.equals(type)).findFirst().get();
    }


}
