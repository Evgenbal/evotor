package com.task.evotor.balakine.evotorTask.common;

import java.io.Serializable;

public abstract class AbstractModel implements Serializable {
    public abstract long getId();
}

