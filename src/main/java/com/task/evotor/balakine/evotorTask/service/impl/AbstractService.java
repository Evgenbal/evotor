package com.task.evotor.balakine.evotorTask.service.impl;

import com.task.evotor.balakine.evotorTask.common.AbstractModel;
import com.task.evotor.balakine.evotorTask.service.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public class AbstractService<T extends AbstractModel> implements Service<T>{
    private static final Logger logger = LoggerFactory.getLogger(AbstractService.class);

    private CrudRepository<T, Long> repository;

    public AbstractService(CrudRepository<T, Long> repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public T create(T obj) {
        logger.debug("start saving entity " + obj);
        T savedEntity = repository.save(obj);
        logger.debug("Entity saved with id " + savedEntity.getId());
        return savedEntity;
    }

    @Override
    public T read(Long id) {
        logger.debug("start find entity with id " + id);
        T foundedEntity = repository.findOne(id);
        logger.debug("Entity found with id " + foundedEntity.getId());
        return foundedEntity;
    }

    @Override
    @Transactional
    public void update(T entity) {
        logger.debug("start update entity " + entity.getId());
        T updatedEntity = repository.save(entity);
        logger.debug("Entity saved with id " + updatedEntity.getId());

    }

    @Override
    @Transactional
    public void delete(Long id) {
        logger.debug("start update entity " + id);
        repository.delete(id);
        logger.debug("Entity deleted with id " + id);

    }

}
