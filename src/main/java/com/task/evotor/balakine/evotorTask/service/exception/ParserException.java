package com.task.evotor.balakine.evotorTask.service.exception;

public class ParserException extends Exception {
    public ParserException(Throwable cause) {
        super(cause);
    }
}
