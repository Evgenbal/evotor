package com.task.evotor.balakine.evotorTask.common.model;

import com.task.evotor.balakine.evotorTask.common.AbstractModel;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"login"}, callSuper = false)
@Builder
@Entity
@Table(name = "account_tbl")
public class Account extends AbstractModel{

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long accountId;

    private String login;

    private String password;

    private double balance;


    @Override
    @Transient
    public long getId() {
        return accountId;
    }
}
