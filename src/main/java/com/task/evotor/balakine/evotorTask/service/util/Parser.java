package com.task.evotor.balakine.evotorTask.service.util;

import com.task.evotor.balakine.evotorTask.common.util.Request;
import com.task.evotor.balakine.evotorTask.common.util.Response;
import com.task.evotor.balakine.evotorTask.service.exception.ParserException;
import com.task.evotor.balakine.evotorTask.service.exception.XmlValidationException;

public interface Parser {
    /**
     * serialize response in xml
     *
     * @param response - response to xml
     * @return xml string
     */

    String marshal(Response response) throws ParserException;

    /**
     * unmarshal Xml file to object
     *
     * @param inputXmlString - xml to parse
     * @return parsed object
     */
    Request unmarshal(String inputXmlString) throws XmlValidationException;
}
