package com.task.evotor.balakine.evotorTask.dao;

import com.task.evotor.balakine.evotorTask.common.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountDao extends CrudRepository<Account, Long> {
    Account findAccountByLogin(String login);
}
