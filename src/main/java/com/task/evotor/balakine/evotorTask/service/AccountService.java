package com.task.evotor.balakine.evotorTask.service;

import com.task.evotor.balakine.evotorTask.common.util.Response;
import com.task.evotor.balakine.evotorTask.common.util.ResponseType;
import com.task.evotor.balakine.evotorTask.common.model.Account;

public interface AccountService extends Service<Account>{

    /**
     * Getting Account from database by login
     *
     * @param accountToFind - Account to find
     * @return founded Account
     */
    Account readByLogin(Account accountToFind);

    /**
     * get balance of user
     * @param account - to find balance
     * @return response with ResponseType
     * @see ResponseType
     */
    Response getBalance(Account account);

    /**
     * registration of new account
     * @param account -
     * @return response with ResponseType
     * @see ResponseType
     */
    Response registration(Account account);
}
