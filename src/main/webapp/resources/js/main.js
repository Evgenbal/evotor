$(document).ready(function () {
    var type = "";
    var login = "";
    var password = "";


    $("#getBalance-submit").on("click", function (e) {
        type = "GET-BALANCE";
        var loginForm = $('#getBalance-form');
        login = loginForm.find('input[name="login"]').val();
        password = loginForm.find('input[name="password"]').val();
        sendRequest(type, login, password);
        e.preventDefault()

    });

    $("#registration-submit").on("click", function (e) {
        type = "CREATE-AGT";

        var registrationForm = $('#registration-form');
        login = registrationForm.find('input[name="login"]').val();
        password = registrationForm.find('input[name="password"]').val();
        sendRequest(type, login, password);
        e.preventDefault()
    });
});

function sendRequest(type, login, password) {

    var str = '<?xml version="1.0" encoding="utf-8"?>' +
        '<request>' +
        '<request-type>' + type + '</request-type>' +
        '<extra name="login">' + login + '</extra>' +
        '<extra name="password">' + password + '</extra>' +
        '</request>';

    $.ajax({
        type: "POST",
        url: "/process",
        data: str,
        contentType: "application/xml",
        dataType: "xml",
        cache: false,
        error: function () {
            alert("No data found.");
        },
        success: function (xmlDoc) {
            console.log(xmlDoc);
            var code = $(xmlDoc).find("result-code").text();
            var extraType = $(xmlDoc).find("extra").attr("name");
            var extraValue = $(xmlDoc).find("extra").text();
            alert("Response Code: " + code);
            if (typeof extraType != 'undefined'){
                alert(extraType + " " + extraValue);
            }


        }
    });

};

