<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <!-- CSS-->


        <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/css/bootstrap.css"
              type="text/css" media="all"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.css"
              type="text/css" media="all"/>

        <link rel='stylesheet' href="${pageContext.request.contextPath}/resources/css/main.css" type='text/css'
              media='all'>
        <!--JS-->

        <script src="${pageContext.request.contextPath}/webjars/jquery/3.3.1/jquery.min.js"
                type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/js/bootstrap.min.js"
                type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/resources/js/main.js" type="text/javascript"></script>

    </head>
    <body>
        <nav class="nav navbar-default navbarStyle">
            <div class="container-fluid">
                <p class="navbar-text "> Evotor Test Task</p>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div role="tabpanel">

                        <ul class="nav nav-tabs" id="tabsEdit">
                            <li>
                                <a href="#getBalance" data-toggle="tab">
                                    Get Balance
                                </a>
                            </li>
                            <li>
                                <a href="#registration" data-toggle="tab">
                                    New User
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="getBalance">
                                <hr>
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="panel panel-proc">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <form id="getBalance-form" style="display: block;">
                                                        <div class="form-group">
                                                            <input type="text" name="login" tabindex="1"
                                                                   class="form-control user" placeholder="Login"
                                                                   value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" name="password"
                                                                   tabindex="2"
                                                                   class="form-control password" placeholder="Password">
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-sm-6 col-sm-offset-3">
                                                                    <input type="button" id="getBalance-submit"
                                                                           tabindex="4"
                                                                           class="form-control btn btn-proc"
                                                                           value="Get Balance">
                                                                    <input type="submit" id="login-submit-forJS" hidden>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="registration">
                                <hr>
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="panel panel-proc">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <c:if test="${not empty error}">
                                                        <div class="error alert alert-danger">
                                                            <span class=" glyphicon glyphicon-remove"></span>
                                                                ${error}</div>
                                                    </c:if>
                                                    <c:if test="${not empty msg}">
                                                        <div class="msg alert alert-success">
                                                            <span class="glyphicon glyphicon-ok-circle"></span>
                                                                ${msg}
                                                        </div>
                                                    </c:if>
                                                    <form id="registration-form" style="display: block;">
                                                        <div class="form-group">
                                                            <input type="text" name="login" tabindex="1"
                                                                   class="form-control user" placeholder="Login"
                                                                   value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="password" name="password"
                                                                   tabindex="2"
                                                                   class="form-control password"
                                                                   placeholder="Password">
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-sm-6 col-sm-offset-3">
                                                                    <input type="button" id="registration-submit"
                                                                           tabindex="4"
                                                                           class="form-control btn btn-proc"
                                                                           value="Registration">
                                                                    <input type="submit" id="registration-submit-forJS"
                                                                           hidden>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>